# ATLAS Docker Files

Dockerfiles for images that contain ATLAS and ML components with [`atlas-sit/docker`](https://gitlab.cern.ch/atlas-sit/docker) as a dependency

- [Dockerfiles](#dockerfiles)
- [Basic Commands Example](#basic-commands-example)
	- [Download containers from Docker Hub](#download-containers-from-docker-hub)
	- [Run a container interactively](#run-a-container-interactively)
	- [Run a Jupyter server and train a NN with TensorFlow](#run-a-jupyter-server-and-train-a-nn-with-tensorflow)
	- [Detaching from the container](#detaching-from-the-container)
	- [Exiting the container](#exiting-the-container)
	- [Run a container non-interactively](#run-a-container-non-interactively)
	- [Running on LXPLUS with Singularity](#running-on-lxplus-with-singularity)
- [Building](#building)
- [Docker Use References](#docker-use-references)

## Dockerfiles

- **ATLAS OS**: CentOS based image of the ATLAS base OS
   - Image name: [`atlasml/centos7-atlasos`](https://hub.docker.com/r/atlasml/centos7-atlasos)
   - Tags: `latest`
- **AnalysisBase**: Image containing the ATLAS AnalysisBase built from the ATLAS OS CentOS image
    - Image name: [`atlasml/centos7-analysisbase`](https://hub.docker.com/r/atlasml/centos7-analysisbase)
    - Tags: `latest`
- **AML Base**: Image containing general use common machine learning libraries built from the AnalysisBase image
   - Image name: [`atlasml/atlasml-base`](https://hub.docker.com/r/atlasml/atlasml-base)
   - Tags: `latest`, `py-3.7.2`, `py-3.6.8`
- **ML Base**: Image containing general use common machine learning libraries
   - Image name: [`atlasml/ml-base`](https://hub.docker.com/r/atlasml/ml-base)
   - Tags: `latest`, `ubuntu`, `bionic`, `py-3.7.2`, `py-3.6.8`, `debian`, `stretch-slim`, `debian-py-3.7.2`, `debian-py-3.6.8`, `centos`, `centos7`, `centos-py-3.7.2`, `centos-py-3.6.8`

## Basic Commands Example

What follows is a very brief example using the ML Base image of how to

- Acquire containers from Docker Hub
- Run containers
- Interactively use containers

### Download containers from Docker Hub

Once you have Docker [installed](https://docs.docker.com/install/) on your local machine pull the desired image from Docker Hub.

```
docker pull atlasml/ml-base:debian
```

Once it downloads you will be able to see it on your local machine by checking your downloaded images

```
docker images
```

which can of course be piped to `grep` as desired.

### Run a container interactively

To run the ML base container in an interactive mode one can run

```
docker run \ # run Docker
    --rm \ # clean up (remove) the container upon exit
    -it \ # shorthand for --interactive --tty (interactive with a pseudo-TTY)
    -v $PWD:/home/atlas/data \ # mount the CWD as a volume to the path inside container given on RHS of ":"
    -p 8888:8888 \ # broadcast on the port given on the RHS of the colon inside container and listen on LHS on host
    atlasml/ml-base:debian # name of the image to run
```

which on one line is

```
docker run --rm -it -v $PWD:/home/atlas/data -p 8888:8888 atlasml/ml-base:debian
```

### Run a Jupyter server and train a NN with TensorFlow

Once the container is running for your own benefit check the versions of Python and `pip` installed

```
~# which python3
/usr/bin/python3
~# python3 --version
Python 3.7.2
~# which pip3
/usr/bin/pip3
~# pip3 --version
pip 19.0.3 from /usr/lib/python3.7/site-packages/pip (python 3.7)
```

Launch a Jupyter server with

```
jupyter notebook
```

Given the Jupyter config file added to the images the server will run in a browserless mode and display a command to copy to your webrowser to connect over the port exposed in the `docker run` command. Once the Jupyter server is running, create a new Jupyter notebook and verify for yourself that imports work as expected

```
import numpy as np
import uproot
import tensorflow as tf
import matplotlib.pyplot as plt
```

and [train a simple neural net with TensorFlow](https://www.tensorflow.org/tutorials/) (`examples/tf_example.py`) to verify things are functional

```
from keras.layers import Input, Flatten, Dense, Dropout
from keras.models import Model

# Load and prepare the MNIST dataset and convert the samples
# from integers to floating-point numbers
mnist = tf.keras.datasets.mnist

(x_train, y_train),(x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

# Build the keras model using the functional API
inputs = Input(shape=x_train.shape[1:])
x = Flatten()(inputs)
x = Dense(512, activation=tf.nn.relu)(x)
x = Dropout(0.2)(x)
predictions = Dense(10, activation=tf.nn.softmax)(x)

model = Model(inputs=inputs, outputs=predictions)
model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

# Train and evaluate the model loss and accuracy
model.fit(x_train, y_train, epochs=5)
model.evaluate(x_test, y_test)
# Save the full model architecture, weights, and optimizer state
model.save('tf_example_model.h5')
```

### Detaching from the container

To [detach](https://docs.docker.com/engine/reference/commandline/attach/#extended-description) from an interactive session of a container and leave it running you can execute the sequential key sequence of `ctrl-p` followed by `ctrl-q`. This returns you to the shell that you were in when you ran the container. To return to the container simply run the [`attach`](https://docs.docker.com/engine/reference/commandline/attach/) command for its container ID/name.

#### Changing the detach key sequence (N.B. emacs users)

The default `ctrl-p`,`ctrl-q` sequence may be annoying for emacs users. To use a user defined detach sequence the [`--detach-keys` option](https://docs.docker.com/engine/reference/commandline/attach/#options) may be passed at container runtime. However, it is probably more useful to [specify the preferred detach key sequence](https://docs.docker.com/engine/reference/commandline/cli/#configuration-files) in your `$HOME/.docker/config.json` file.

An an example, the following could be appended to the `$HOME/.docker/config.json`

```
"detachKeys" : "ctrl-d,ctrl-q"
```

### Exiting the container

To exit the container once you have killed the Jupyter server just type `exit` as you would to exit a normal shell. You can verify that the Docker container was cleaned up by noticing that it doesn't appear in `docker ps -a`.

### Run a container non-interactively

You can of course also have Docker containers execute code non-interactively by passing in commands to [`docker run`](https://docs.docker.com/engine/reference/run/)

```
docker run <image name>:<image tag> <command>
```

with multiple commands being run in a shell separated by `;` or `&&`

```
# Using Bash as the shell
docker run <image name>:<image tag> /bin/bash -c <commands>
```

As an example, run the NN training with TensorFlow example (`examples/tf_example.py`) from earlier

```
docker run --rm \
	-v $PWD:/home/atlas/data \
	atlasml/ml-base:debian \
	/bin/bash -c "curl https://gitlab.cern.ch/aml/containers/docker/raw/master/examples/tf_example.py | python3; printf '\nFinished example\n'"
```

### Running on LXPLUS with Singularity

LXPLUS and some GRID sites do not have Docker installed on them, and so [Singularity](https://www.sylabs.io/singularity/) must be used to run these images on those sites. Additionally, only the LXPLUS 7 sites (`lxplus7.cern.ch`) have the required libraries to be able to run Ubuntu based images, so if you are on LXPLUS then you must use the CentOS 7 based images.

Before doing anything with Singularity it is advised to set your `SINGULARITY_CACHEDIR` to be in `/tmp` so that you don't pollute your home environment. You can do this with the following

```
printf '\nexport SINGULARITY_CACHEDIR=/tmp/$USER\n' >> ~/.bashrc
```

To then run any image in an interactive session with Singularity you can pull it down and start an interactive session with [`singularity shell`](https://www.sylabs.io/guides/3.0/user-guide/quick_start.html?highlight=shell#shell)

```
singularity shell -e docker://<image name>:<image tag>
```

So, for the CentOS 7 based version of the ML base image

```
singularity shell -e docker://atlasml/ml-base:centos-py-3.6.8
```

and if the image desired is being hosted on a GitLab registry then that path information need to be added to the image name. For example,

```
singularity shell -e docker://gitlab-registry.cern.ch/aml/containers/docker/atlasml/ml-base:centos-py-3.6.8
```

There are also times when you want to run code in the environment with Singularity, but not interactively. For this you can use [`exec`](https://www.sylabs.io/guides/3.0/user-guide/quick_start.html#executing-commands) to execute commands inside the container, and then exit and remove the container

```
singularity exec -e docker://<image name>:<image tag> <command>
```

with multiple commands being run in a shell separated by `;` or `&&`

```
# Using Bash as the shell
singularity exec -e docker://<image name>:<image tag> /bin/bash -c <commands>
```

As an example, run the NN training with TensorFlow example (`examples/tf_example.py`)

```
singularity exec -e docker://atlasml/ml-base:centos-py-3.6.8 \
	/bin/bash -c "curl https://gitlab.cern.ch/aml/containers/docker/raw/master/examples/tf_example.py | python3; printf '\nFinished example\n'"
```

## Building

To build all the images simply run

```
make
```

from the top level directory of the repo. To make specific image check the contents of the `Makefile` for the target you want.

## Docker Use References

There are several useful places to get started using Docker for HEP:

 - [Matthew's Intro to Docker](https://github.com/matthewfeickert/Intro-to-Docker)
 - [Lukas's ATLAS Docker Tutorial](https://indico.cern.ch/event/757797/contributions/3141901/)
